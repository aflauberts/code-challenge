# Instalação

Olá, o desafio foi escrito com **Ruby on Rails** e banco de dados **PostgresSQL**


### Pre-Requisitos
Linguagem Ruby versão 2.4.2 +
PostgresSQL 9.4 +

## Preparando o projeto

Após realizar o clone do repositório, dentro da pasta do projeto:

> Altere a versão do Ruby no arquivo Gemfile, na raiz do projeto.

execute: `bundle install` para instalar as dependências (Gems) utilizadas no projeto.

#### Configurando e criando o  banco de dados:

> Altere as credenciais de acesso ao banco de dados no arquivo **db/database.yml**

Execute `rake db:create` para criar o banco de dados.

Execute `rake:db:migrate` para criar as tabelas 

Execute `rails db:seed`para popular o banco de dados com dados de exemplo

Para levantar o servidor:

  execute   `rails s`

Será disponibilizado em http://localhost:3000/

## Chamando os serviços

Acesse a [Documentação](https://documenter.getpostman.com/view/6076120/SVSHqpE4?version=latest)
