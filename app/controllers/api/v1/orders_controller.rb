module Api
  module V1

    class OrdersController < ApplicationController
      def index
        orders = Order.all.order(:id)
        render json: orders, :include => {:order_items => {:include => :product}, :status => {:include => {}}}, status: :ok
      end

      def show
        order = Order.where(:id => params[:id])
        render json: order, :include => {:order_items => {:include => :product}, :status => {:include => {}}}, status: :ok
      end

      def create
        order = Order.new(order_params)
        if order.save
          render json: order,:include => [:order_items, :status],  status: :ok
        else
          render json: order.errors.full_messages, status: :unprocessable_entity
        end
      end

      def destroy
				order = Order.find(params[:id])
				order.destroy
        render json: order, status: :ok
			end
		
			def update
        order = Order.find(params[:id])
        if order.update_attributes(order_params) && order.save
          render json: order, :include => {:order_items => {:include => :product}, :status => {:include => {}}}, status: :ok
        else
          render json: order.errors.full_messages,status: :unprocessable_entity
        end
      end

      private
      def order_params
        params.require(:order).permit(
            :id, 
            :customer, 
            :delivery, 
            :date, 
            :status_id,
            order_items_attributes: [
              :id, 
              :product_id, 
              :price, 
              :quantity,
              :_destroy
            ]
        )
      end
    end
  end
end

