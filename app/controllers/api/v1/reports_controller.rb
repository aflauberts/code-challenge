module Api
  module V1

    class ReportsController < ApplicationController
      def ticket
        if params.has_key?(:ini_date) && params.has_key?(:end_date)
          ticket_avg = Order.includes(:order_items, :status).where('orders.created_at BETWEEN ? and ? and statuses.description LIKE ?',params[:ini_date] , params[:end_date], 'Aprovado').average(:price)
          render json: [:ticket => ticket_avg.to_f.to_d], status: :ok
        else
          render json: [:ticket => ticket_avg], status: :unprocessable_entity
        end
      end
    end
  end
end

