module Api
  module V1
    class ProductsController < ApplicationController

      def index
        products = Product.all
        render json: products, :include => [:characteristics], status: :ok
      end

      def show
        product = Product.find(params[:id])
        render json: product,:include => [:characteristics], status: :ok
      end

      def create
        product = Product.new(product_params)
        if product.save
          render json: product,:include => [:characteristics],  status: :ok
        else
          render json: product.errors, status: :unprocessable_entity
        end
      end

      def destroy
				product = Product.find(params[:id])
				product.destroy
        render json: product, status: :ok
			end
		
			def update
				product = Product.find(params[:id])
        if product.update_attributes(product_params)
          product = Product.find(params[:id])
          render json: product,:include => [:characteristics],status: :ok
				else
					render json: product.erros,status: :unprocessable_entity
				end
			end

      private
      def product_params
        params.require(:product).permit(
            :name, :description, :quantity, :price,
            characteristics_attributes: [:id, :key, :value]
        )
      end
    end
  end
end