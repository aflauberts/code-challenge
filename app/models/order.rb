class Order < ApplicationRecord
  belongs_to :status
  has_many :order_items, :dependent => :destroy
  has_many :products, through: :order_items

  validates :status, :presence => true
  validates :order_items, :presence => true

  accepts_nested_attributes_for :status
  accepts_nested_attributes_for :order_items, allow_destroy: true

  before_create :default_values

  def default_values
    self.date = Time.now
    self.status = Status.first
  end


end
