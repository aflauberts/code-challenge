class Product < ApplicationRecord
  has_many :characteristics, :dependent => :destroy

  accepts_nested_attributes_for :characteristics
  
  validates :name, presence: true
  validates :description, presence: true
  validates :price, presence: true
  validates :quantity, presence: true
end
