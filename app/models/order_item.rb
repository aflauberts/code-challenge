class OrderItem < ApplicationRecord
  belongs_to :product
  belongs_to :order

  validates :product, presence: { message: "does not exists" }
  validates :order, :presence => true
  validate :has_no_stock

  after_save :decrement_product_quantity!

  #decrementa qtd do produto
  def decrement_product_quantity!
    self.product.decrement!(:quantity, quantity)
  end

  #checa estoque
  def has_no_stock
   if product.present?
      if quantity > product.quantity
        errors.add(product.name, "has no sufficient stock, #{product.quantity} left")
      end
   end
  end

end
