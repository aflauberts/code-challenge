class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.references :status, :default => 1, foreign_key: true
      t.string :customer
      t.float :delivery
      t.timestamp :date

      t.timestamps
    end
  end
end
