class CreateCharacteristics < ActiveRecord::Migration[5.2]
  def change
    create_table :characteristics do |t|
      t.belongs_to :product, foreign_key: true
      t.string :key
      t.string :value

      t.timestamps
    end
  end
end
