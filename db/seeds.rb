# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#criando os produtos (livros)
 10.times do
   Product.create({
                      name: Faker::Book.title,
                      description: Faker::Lorem.sentence,
                      price: Faker::Number.decimal(2),
                      quantity: Faker::Number.number(3)
                  })
 end

products = Product.all
products.each do |p|
  Characteristic.create({
                            product_id: p.id,
                            key: 'color',
                            value: Faker::Color.color_name,
                            
                        })
end

#criando os status
 Status.create([{description: 'Novo'}, {description: 'Aprovado'}, {description: 'Entregue'}, {description: 'Cancelado'}])

#criando  orders
