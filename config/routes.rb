Rails.application.routes.draw do
  namespace 'api' do
  	namespace 'v1' do
  		resources :products, :orders
			get 'reports/(*)', to: 'reports#ticket'


  	end
  end

end
