require 'rails_helper'

RSpec.describe Order, type: :model do

  describe "validations" do
    it {should validate_presence_of(:status)}
    it {should validate_presence_of(:order_items)}
  end

  describe 'associations' do
    it {should have_many(:order_items).dependent(:destroy)}
    it {should have_many(:products)}
  end

end
