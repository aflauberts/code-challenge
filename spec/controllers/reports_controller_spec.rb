require 'rails_helper'

RSpec.describe Api::V1::ReportsController, type: :controller do


  describe "GET #ticket success" do
    before do
      get :ticket, params: {ini_date: '2019-01-01', end_date:'2019-12-31'}
    end
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
  end
end


