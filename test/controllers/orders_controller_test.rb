require 'test_helper'

class OrdersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get api_v1_orders_url
    assert_response :success
  end

  test "should get create" do
    params = '{\r\n    \"order\": {\r\n        \"customer\": \"TEST Customer\",\r\n        \"delivery\": 2.9,\r\n        \"order_items_attributes\": [\r\n            {\r\n                \"product_id\": 2,\r\n                \"price\": \"50.9\",\r\n                \"quantity\": 3\r\n            }\r\n        ]\r\n    }\r\n}'
    post api_v1_orders_url, params.to_json, {'ACCEPT' => "application/json", 'CONTENT_TYPE' => 'application/json'}

    assert_response :success
  end

  test "should get update" do
    get orders_update_url
    assert_response :success
  end

end
